package com.testARLs.testframework;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.List;


public class UserActions {
    static WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser() {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    public static void clickElement(String key) {
        Utils.LOG.info("Clicking on element " + key);
        waitForElementVisible(key, 10);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        element.click();
    }

    public static void clickCSSElement(String key) {
        Utils.LOG.info("Clicking on element " + key);
        waitForElementVisible(key, 10);
        WebElement element = driver.findElement(By.cssSelector(Utils.getUIMappingByKey(key)));
        element.click();
    }
    public static void clickIdElement(String key) {
        Utils.LOG.info("Clicking on element " + key);
        waitForElementVisible(key, 10);
        WebElement element = driver.findElement(By.id(Utils.getUIMappingByKey(key)));
        element.click();
    }
    public void clickOnHiddenElement(String key) {
        Utils.LOG.info("Clicking on hiding element " + key);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement Element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        js.executeScript("arguments[0].click();", Element);
    }

    public void typeValueInField(String value, String field) {
        Utils.LOG.info("Typing " + value + " in " + field);
        clearField(field);
        WebElement Element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        Element.sendKeys(value);
    }

    public void clearField(String field) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        element.sendKeys(Keys.CONTROL + "a");
        element.sendKeys(Keys.BACK_SPACE);
    }

    public void scrollByVisibleElement(String key) {
        Utils.LOG.info("Scrolling until find the element " + key);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement Element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        js.executeScript("arguments[0].scrollIntoView();", Element);
    }

    //############# WAITS #########
    public static void waitForElementVisible(String locator, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public static void waitForCSSElementVisible(String locator, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(Utils.getUIMappingByKey(locator))));
    }
    public static void waitForIdElementVisible(String locator, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Utils.getUIMappingByKey(locator))));
    }

    public void waitForElementClickable(String locator, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    //############# ASSERTS #########
    public static void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public static void assertCSSElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.cssSelector(Utils.getUIMappingByKey(locator))));
    }

    public static void assertElementNotPresent(String locator) {
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
    }

    public void assertTextEquals(String expectedText, String locator){
        WebDriverWait wait= new WebDriverWait(driver,10);
        WebElement webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
        String textOfWebElement = webElement.getText();
        Utils.LOG.info("Expected text: " + expectedText + "; Actual text: " + textOfWebElement);
        Assert.assertEquals(expectedText, textOfWebElement);
    }

    //############# LOGIN ##########
    public void logIn() {
        Utils.LOG.info("Waiting until login page is loaded");
        waitForElementVisible("loginPageTitle", 5);
        assertElementPresent("mcoLogo");
        assertElementPresent("mcoAuthPageBackground");
        assertElementPresent("usernameLabel");
        assertElementPresent("passwordLabel");
        try {
            typeValueInField(Utils.getConfigPropertyByKey("username"), Utils.getUIMappingByKey("usernameTextField"));
			typeValueInField(Utils.getConfigPropertyByKey("password"), Utils.getUIMappingByKey("passwordTextField"));
        }
		catch (Exception exception){
			Utils.LOG.info("Already signed in");
		}
		clickElement(Utils.getUIMappingByKey("loginBtn"));
		Utils.LOG.info("Login button is clicked");
    }
    public void logOut() {
        Utils.LOG.info("User is logging out.");
        waitForElementVisible("profileSection", 5);

        clickElement("logoutBtn");
        driver.navigate().refresh();
    }
    public String getRandomString(int n) {

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

}