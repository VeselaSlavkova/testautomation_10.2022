package TestARLs;

import com.testARLs.testframework.UserActions;
import com.testARLs.testframework.Utils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
public class HomepageSteps extends Base{
    public static void verifyHomepageScreen(){
        Utils.LOG.info("Homepage is opening");

        actions.waitForElementVisible("homepageHeader", 5);
        Utils.LOG.info("00");
        actions.assertElementPresent("navigationMenuBar");
        Utils.LOG.info("01");
        actions.assertElementPresent("homeTab");
        Utils.LOG.info("02");
        actions.assertElementPresent("myTab");
        actions.assertElementPresent("kyeTab");
        actions.assertElementPresent("kytTab");
        actions.assertElementPresent("drmTab");
        actions.assertElementPresent("kytpKycTab");
        actions.assertElementPresent("ermTab");
        actions.assertElementPresent("toolsTab");
        actions.assertElementPresent("adminTab");
        actions.assertElementPresent("kyrTab");
    }
}
