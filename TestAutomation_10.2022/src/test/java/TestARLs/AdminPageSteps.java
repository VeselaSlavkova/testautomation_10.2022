package TestARLs;

import com.testARLs.testframework.UserActions;
import com.testARLs.testframework.Utils;
import org.openqa.selenium.By;

public class AdminPageSteps extends Base {
    public static void verifyAdminPageScreen() {
        Utils.LOG.info("Admin page is opening");
        actions.waitForElementVisible("adminTab", 20);
        actions.clickElement("adminTab");
        actions.assertElementPresent("adminSettings");
        actions.assertTextEquals("SETTINGS","adminSettings");
        actions.assertElementPresent("adminFirmData");
        actions.assertTextEquals("FIRM DATA","adminFirmData");
        actions.assertElementPresent("adminMaster");
        actions.assertTextEquals("MASTER","adminMaster");
    }

    public static void verifyMCOMasterDataTab() {
        String sectionPersonARLs = "Person ARLs";
        String sectionCLEARLs = "Company Legal Entity (CLE) ARLs";
        Utils.LOG.info("MCOMasterData section is opening");

        actions.assertElementPresent("regulatoryAuthoritiesSection");
        actions.assertElementPresent("CLEARLsSection");
        actions.assertTextEquals(sectionCLEARLs, "CLEARLsSection" );
        actions.assertElementPresent("personARLsSection");
        actions.assertTextEquals(sectionPersonARLs,"personARLsSection");
        actions.assertCSSElementPresent("examinationsSection");
        actions.assertCSSElementPresent("CEStructuresSection");
        actions.assertCSSElementPresent("examProvidersSection");
    }


}
