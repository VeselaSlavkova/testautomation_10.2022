package TestARLs;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CLEARLsTest extends Base {

    @Test
    public void test_01_CLEARLsListingPage() {
        actions.logIn();
        AdminPageSteps.verifyAdminPageScreen();
        CLEARLsSteps.verifyCLEARLsPage();
        CLEARLsSteps.verifyCLEARLsListing();
    }
   @Test
   public void test_02_CLEARLsCOGOptions() {
       CLEARLsSteps.verifyCLEARLsCOGOptions();
   }
//    @Test
//    public void test_03_FullCLEARLsListingPage() {
//        CLEARLsSteps.verifyFullCLEARLsListing();
//    }
//    @Test
//    public void test_04_CLEARLsCOGChecks() {
//        CLEARLsSteps.verifyChecksInCOG();
//        CLEARLsSteps.verifyCLEARLsListing();
//    }
//    @Test
//    public void test_05_LEARLsDetailsPage() {
//     //   CLEARLsSteps.verifyCLEARLsDetailsPage();
//        CLEARLsSteps.addNewLEARL();
//        CLEARLsSteps.reportForNewCLEARL();
//    }
}