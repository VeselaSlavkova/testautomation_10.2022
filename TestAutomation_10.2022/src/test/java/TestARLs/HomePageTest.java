package TestARLs;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HomePageTest extends  Base{

    @Test
    public void test_01_Homepage() {
        actions.logIn();
        HomepageSteps.verifyHomepageScreen();
    }
}
