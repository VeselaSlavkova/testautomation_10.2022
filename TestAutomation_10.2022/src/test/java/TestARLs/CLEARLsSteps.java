package TestARLs;

import com.testARLs.testframework.Utils;

public class CLEARLsSteps extends  Base {
    static String regulator_authBodies = "Regulators / Authorizing Bodies / Administrators";
    static String regulatory_Authority = "Regulatory Authority/Administrative Bodies";
    static String regulator_authorizer = "Regulator/Authorizer";
    static String LEName = "LE Name";
    static String Code = "Code";
    static String LEARLCode = "LE ARL Code";
    static String Name = "Name";
    static String ARLCode = "ARL Code";
    static String continuingEducation = "Continuing Education";
    static String description = "Description";
    static String website = "Website";
    static String lengthOfTermsOfValidityForAuth = "Length of term of validity for the authorization";
    static String canHaveMultiplePersonAuth = "Can have multiple person authorizations for this authorization";
    static String currency = "Currency";
    static String customDescrForCompanyAuth = "Allow custom description text for a company authorization";
    static String requireAuthBeEntered = "Require Authorization ID be entered when adding a User Authorization";
    static String fees = "Fees";
    static String businessRulesForAuth = "Business Rules Assoc with this Auth";
    static String requiredNumberOfExams = "Required Number of Exams";
    static String isExamRequiredBeforeARL = "Is an exam required before ARL is applied for?";
    static String arePrerequisiteExamsRequired = "Are Prerequisite Exams required?";
    static String requirementToDifferResidentsPerARL = "Is there a requirement to differentiate between resident and non-resident per ARL?";
    static String aggregateByUSState = "Aggregate by US State";
    static String showAll = "Show all";

    static String parentCLEARL = "Parent CLE ARL";

    public static void verifyCLEARLsPage() {
        String title = "Company Legal Entity (CLE) ARLs";

        actions.clickElement("adminFirmData");
        try {
            actions.clickElement("CLEARLsSection");
            Utils.LOG.info("CLE ARLs page is opening");
        } catch (Exception exception) {
            Utils.LOG.info("CLE ARLs page cannot be open");
        }
        actions.waitForElementVisible("CLEARLsTitle", 5);
        actions.assertTextEquals(title, "CLEARLsTitle");
        actions.assertElementPresent("CLEARLsListTitle");
        actions.assertTextEquals(title, "CLEARLsListTitle");
    }

    public static void verifyCLEARLsListing() {

        Utils.LOG.info("CLE ARLs listing is displaying");
        actions.assertElementPresent("CLEARLsListing");
        actions.assertTextEquals(regulator_authorizer, "CLE_regulator_authorizer");
        actions.assertTextEquals(LEName, "LEName");
        actions.assertTextEquals(LEARLCode, "LEARLCode");
    }

    public static void verifyCLEARLsCOGOptions() {
        actions.assertElementPresent("COGButtonCLE");
        Utils.LOG.info("COGButtonCLE is displayed");
        try {
            actions.clickElement("COGButtonCLE");
        } catch (Exception exception) {
            Utils.LOG.info("COGButtonCLE is not active");
        }
        actions.waitForElementVisible("COGDropdownCLE", 5);
        actions.assertTextEquals(regulator_authBodies, "regulators_authBodies");
        actions.assertTextEquals(regulator_authorizer, "COG_regulator_authorizer");
        actions.clickElement("checkboxRegulator_authorizer");
        actions.assertTextEquals(LEName, "COG_LEName");
        //   actions.clickElement("checkboxCLEName");
        actions.assertTextEquals(Code, "COG_Code");
        //  actions.clickElement("checkboxShortName");
        actions.assertTextEquals(LEARLCode, "COG_LEARLCode");
        // actions.clickElement("checkboxCLEARLCode");
        actions.assertTextEquals(continuingEducation, "COG_continuingEducation");
        actions.assertTextEquals(description, "COG_description");
        actions.assertTextEquals(website, "COG_website");
        actions.assertTextEquals(lengthOfTermsOfValidityForAuth, "lengthOfTermsOfAuth");
        actions.scrollByVisibleElement("canHaveMultiplePersonAuth");
        actions.assertTextEquals(canHaveMultiplePersonAuth, "canHaveMultiplePersonAuth");
        //    actions.clickElement("checkboxCanHaveMultiplePersonAuth");
        actions.scrollByVisibleElement("COG_currency");
        actions.assertTextEquals(currency, "COG_currency");
        //  actions.clickElement("checkboxCurrency");
        actions.scrollByVisibleElement("custDescrForCompanyAuth");
        actions.assertTextEquals(customDescrForCompanyAuth, "custDescrForCompanyAuth");
        //   actions.clickElement("checkboxCustDescrForCompanyAuth");
        actions.scrollByVisibleElement("requireAuthBeEntered");
        actions.assertTextEquals(requireAuthBeEntered, "requireAuthBeEntered");
        //     actions.clickElement("checkboxRequireAuthBeEntered");
        actions.scrollByVisibleElement("COG_fees");
        actions.assertTextEquals(fees, "COG_fees");
        //  actions.clickElement("checkboxFees");
        actions.scrollByVisibleElement("businessRulesForAuth");
        actions.assertTextEquals(businessRulesForAuth, "businessRulesForAuth");
        //  actions.clickElement("checkboxBusinessRulesForAuth");
        actions.scrollByVisibleElement("requiredNumberOfExams");
        actions.assertTextEquals(requiredNumberOfExams, "requiredNumberOfExams");
        //     actions.clickElement("checkboxRequiredNumberOfExams");
        actions.scrollByVisibleElement("isExamRequiredBeforeARL");
        actions.assertTextEquals(isExamRequiredBeforeARL, "isExamRequiredBeforeARL");
        //   actions.clickElement("checkboxIsExamRequiredBeforeARL");
        actions.scrollByVisibleElement("prerequisiteExamsRequired");
        actions.assertTextEquals(arePrerequisiteExamsRequired, "prerequisiteExamsRequired");
        //    actions.clickElement("checkboxPrerequisiteExamsRequired");
        actions.scrollByVisibleElement("requirementToDifferResidentsPerARL");
        actions.assertTextEquals(requirementToDifferResidentsPerARL, "requirementToDifferResidentsPerARL");
        //     actions.clickElement("checkboxRequirementToDifferResidentsPerARL");
        actions.scrollByVisibleElement("aggregateByUSState");
        actions.assertTextEquals(aggregateByUSState, "aggregateByUSState");
        // actions.clickElement("checkboxAggregateByUSState");
        actions.scrollByVisibleElement("COG_ShowAll");
        actions.assertTextEquals(showAll, "COG_ShowAll");
        actions.clickElement("checkboxShowAll");
        actions.clickElement("CLEARLsListing");
    }

    public static void verifyFullCLEARLsListing() {
        Utils.LOG.info("CLE ARLs listing is displaying");
        actions.assertElementPresent("CLEARLsListing");
        actions.assertTextEquals(regulator_authBodies, "CLE_regulator_authBodies");
//        actions.assertTextEquals(regulator_authorizer,"CLE_regulator_authorizer");
//        actions.assertTextEquals(CLEName,"CLEName");
//        actions.assertTextEquals(CLEARLCode,"CLEARLCode");
    }

    public static void verifyChecksInCOG() {
        Utils.LOG.info("COG dropdown is displayed");
        actions.clickElement("COGButtonCLE");
        actions.clickElement("checkboxRegulators_authBodies");
        actions.clickElement("checkboxShortName");
        actions.clickElement("checkboxContinuingEducation");
        actions.clickElement("checkboxDescription");
        actions.clickElement("checkboxWebsite");
        actions.clickElement("checkboxLengthOfTermsOfAuth");
        actions.scrollByVisibleElement("canHaveMultiplePersonAuth");
        actions.clickElement("checkboxCanHaveMultiplePersonAuth");
        actions.scrollByVisibleElement("COG_currency");
        actions.clickElement("checkboxCurrency");
        actions.scrollByVisibleElement("custDescrForCompanyAuth");
        actions.clickElement("checkboxCustDescrForCompanyAuth");
        actions.scrollByVisibleElement("requireAuthBeEntered");
        actions.clickElement("checkboxRequireAuthBeEntered");
        actions.scrollByVisibleElement("COG_fees");
        actions.clickElement("checkboxFees");
        actions.scrollByVisibleElement("businessRulesForAuth");
        actions.clickElement("checkboxBusinessRulesForAuth");
        actions.scrollByVisibleElement("requiredNumberOfExams");
        actions.clickElement("checkboxRequiredNumberOfExams");
        actions.scrollByVisibleElement("isExamRequiredBeforeARL");
        actions.clickElement("checkboxIsExamRequiredBeforeARL");
        actions.scrollByVisibleElement("prerequisiteExamsRequired");
        actions.clickElement("checkboxPrerequisiteExamsRequired");
        actions.scrollByVisibleElement("requirementToDifferResidentsPerARL");
        actions.clickElement("checkboxRequirementToDifferResidentsPerARL");
        actions.scrollByVisibleElement("aggregateByUSState");
        actions.clickElement("checkboxAggregateByUSState");
        actions.clickElement("CLEARLsListing");
    }

    public static void verifyCLEARLsDetailsPage() {
        actions.waitForElementClickable("CLERecordInNewTabLink", 5);
        try {
            actions.clickElement("CLERecordInNewTabLink");
        } catch (Exception exception) {
            Utils.LOG.info("Selected record cannot be open");
        }
        Utils.LOG.info("CLE ARLs details page is opening in new tab");
        //actions.assertCSSElementPresent("detailsTitle");
        actions.assertTextEquals(regulatory_Authority, "det_regulators_authBodies");
        actions.assertTextEquals(ARLCode, "det_LEARLCode");
        actions.assertTextEquals(Name, "det_LEName");
        actions.assertTextEquals(Code, "det_Code");
        actions.assertTextEquals(parentCLEARL, "det_ParentCLEARL");
        actions.assertElementNotPresent("personARLsSubSection");
        actions.assertElementPresent("configurationOptionsTitle");
        actions.clickElement("returnToListButton");
    }

    public static void addNewLEARL() {
        actions.waitForElementVisible("CLEARLsListing", 5);
        try {
            actions.clickElement("addACLEARLButton");
        } catch (Exception exception) {
            Utils.LOG.info("Add a New CLE ARL page is opening");
        }
        actions.assertElementPresent("addNewLEARLTitle");
        actions.assertTextEquals("Add New LE ARL","addNewLEARLTitle");
        actions.clickElement("dropdown_regulators_authBodies");
        actions.clickElement("forth_regulators_authBodies");

        actions.typeValueInField(actions.getRandomString(3), "field_LEARLCode");
        actions.typeValueInField(actions.getRandomString(3), "field_LEARLName");
        actions.typeValueInField(actions.getRandomString(3), "field_LECode");
        actions.clickElement("dropdownCLEPersonARL");
        actions.clickElement("firstCLEPersonARL");
        actions.scrollByVisibleElement("submitButton");
        actions.clickElement("submitButton");
    }

    public static void reportForNewCLEARL() {
        actions.waitForElementVisible("CLEARLsListing", 5);
        try {
            actions.clickElement("reportsButton");
        } catch (Exception exception) {
            Utils.LOG.info("Report for new CLE ARL starting");
        }
        actions.clickElement("includeAllFieldsReportCheckbox");
        actions.clickElement("downloadButton"); //verify the report manually
        actions.clickElement("createdCLEARLbyCode");
       // actions.assertElementPresent("detailsTitle");
        actions.clickElement("historyTertiaryMenu");
        actions.assertElementPresent("historyTitle");
        actions.assertElementPresent("historyAction");
        actions.clickElement("returnToListButton");
    }
}