package TestARLs;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MasterPersonARLsTest extends Base {

    @Test
    public void test_01_MasterPersonARLsPage() {
        actions.logIn();
        AdminPageSteps.verifyAdminPageScreen();
        MasterPersonARLsSteps.verifyPersonARLsPage();
        MasterPersonARLsSteps.verifyPersonARLsListing();
    }

    @Test
    public void test_02_MasterPersonARLsDetailsPage() {
        MasterPersonARLsSteps.verifyPersonARLsDetailsPage();
    }

}
