package TestARLs;

import com.testARLs.testframework.Utils;

public class MasterPersonARLsSteps extends Base{
    public static void verifyPersonARLsPage() {
        String pageTitle = "Master Person ARLs (Authorizations, Registrations, Licenses)";
        try {
            actions.clickElement("personARLsSection");
            Utils.LOG.info("PersonARLs page is opening");
        } catch (Exception exception) {
            Utils.LOG.info("PersonARLs page cannot be open");
        }
        actions.waitForElementVisible("personARLsTitle", 5);
        actions.assertTextEquals(pageTitle,"personARLsTitle");
        actions.assertElementPresent("personARLsListTitle");
        actions.assertTextEquals(pageTitle,"personARLsListTitle");
           }

    public static void verifyPersonARLsListing(){
        String regulator_authorizer = "Regulator/Authorizer";
        String ARLName = "ARL Name";
        String ARLCode = "ARL Code";
        Utils.LOG.info("Master PersonARLs listing is displaying");
        actions.assertElementPresent("personARLsListing");
        actions.assertTextEquals(regulator_authorizer,"regulator_authorizer");
        actions.assertTextEquals(ARLName,"ARLName");
        actions.assertTextEquals(ARLCode,"ARLCode");
    }

    public static void verifyPersonARLsDetailsPage(){
        Utils.LOG.info("PersonARL details page is opening");
        try {
            actions.clickElement("firstExistingRecord");
        } catch (Exception exception) {
            Utils.LOG.info("Selected record cannot be open");
        }

        actions.assertElementNotPresent("personARLsSubSection");
    }

}
