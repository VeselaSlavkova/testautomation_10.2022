package TestARLs;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdminPageTest extends Base{

    @Test
    public void test_01_AdminPageSubMenu() {
        actions.logIn();
        AdminPageSteps.verifyAdminPageScreen();
    }
    @Test
    public void test_02_AdminMCOMasterData() {
        AdminPageSteps.verifyMCOMasterDataTab();
    }
}
